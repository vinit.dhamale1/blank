 addtocartcron.py : This scripts goes through stepuplogs.audit_trails and checks what all product customers have been purchasing at various period level.
 1. weekly
 2. Monthly 
 3 .Daily 
This gets calculated at store level and we store in to the table named addtocarttrend, this can be used to show trending new products to customers.
-----------------------------------------------------------------------------------------------------------------
itempermute.py :
This script helps to identify which product combos are trending at each within store, with other stores and which gives us idea that which famous combos can be promoted together

-----------------------------------------------------------------------------------------------------------------

monthly_customer_patterns.py :
This script gives us the idea about customer purchase behaviour , we calcualte monthonmonthavgordersize for each customer.

----------------------------------------------------------------------------------------------------------------


productcombossoldmost.py : 
Best product combos calculation happens through this script. We also calculate correlation between 2 products. 

-------------------------------------------------------------------------------------------------------------

sale_by_vendor_product_customer.py :
This report gives the idea about how many products are getting sold by each store and for how much cost and for which period. This was utilized by Rahul sir for money release for vendor from our side.

--------------------------------------------------------------------------------------------------------------

vendordashboardcompute.py :
We are calculating the sales for each vendor across different cities and how they are doing comparitively previous months.
