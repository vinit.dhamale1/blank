# -*- coding: utf-8 -*-
"""sale by vendor,product,customer.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1jBCF5q3x6EGDhVKmdN5acFJT7VcbA3vb
"""


from sqlalchemy import create_engine
import pandas as pd
engine = create_engine("mysql+pymysql://tuadmin:Turant123@turantdb-instance.cdvlyair641e.ap-south-1.rds.amazonaws.com""/turant")
connn = engine.connect()


# products= pd.read_sql("""SELECT item_id FROM (select item_id,count(item_id) itemcount from order_items
# where created_on > curdate() - interval 365 day 
# group by item_id ) AS A
# order by itemcount desc;""",con=connn)


details= pd.read_sql("""SELECT distinct orders.order_group_no,order_deliveries.delivery_name,
concat(user_info.f_name,' ',user_info.l_name) as `Customer Name`, user_auth.mobile_no ,
orders.created_on, order_deliveries.delivery_partner_name, item_id,item_name,mrp_mrp,stores.name
FROM order_delivery_items
inner join order_deliveries on order_deliveries.id= order_delivery_items.order_delivery_id
inner join orders on order_delivery_items.order_id=orders.id
inner join stores on orders.store_id=stores.id
inner join store_delivery_partners on stores.id = store_delivery_partners.store_id
inner join delivery_partners on store_delivery_partners.delivery_partner_id = delivery_partners.id
inner join user_addresses on user_addresses.user_id=orders.user_id
inner join user_info on user_info.id=orders.user_id 
inner join user_auth on orders.user_id=user_auth.user_id
inner join products on products.id=order_delivery_items.item_id
where (orders.payment_status=1)
and orders.created_on >= '01/01/21'
order by order_deliveries.delivery_date;""",con=connn)

details.to_csv('details.csv')