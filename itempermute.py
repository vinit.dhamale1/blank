from sqlalchemy import create_engine
import pandas as pd
engine = create_engine("mysql+pymysql://tuadmin:Turant123@turantdb-instance.cdvlyair641e.ap-south-1.rds.amazonaws.com""/turant")
connn = engine.connect()

"""**within Product**"""

storename= pd.read_sql("""SELECT name FROM (SELECT stores.name,sum(gross_total) as total FROM orders
left join stores on stores.id=orders.store_id
where orders.created_on> curdate() - interval 30 day
group by stores.name) AS A
order by total desc limit 5;""",con=connn)

def withproduct(v):
  df = pd.read_sql("""SELECT products.name,item_qty FROM order_items
  left join products on order_items.item_id=products.id
  left join orders on orders.id=order_items.order_id
  left join stores on stores.id=orders.store_id
  where -- item_qty > 1 and 
  stores.name= "{v}";
    """.format(v=v), con=connn)
  df['itemqtycat']=df['item_qty'].astype(str)
  frequency = pd.crosstab(df['name'],df['item_qty'])
  numberlabels= [int(item) for item in frequency.columns]
  volume= pd.pivot_table(df,index='name',columns='itemqtycat',aggfunc='sum').fillna(0).apply(lambda r: (r/r.sum())*100, axis=1).iloc[:,1:].reset_index()
  volume['Co-Order %age within same Order in Total Sales (Units) (In Store)']= volume.sum(axis=1)
  volume= volume[['name','Co-Order %age within same Order in Total Sales (Units) (In Store)']]
  frequency=pd.pivot_table(df,index='name',columns='itemqtycat',aggfunc='count').fillna(0).apply(lambda r: (r/r.sum())*100, axis=1).iloc[:,1:].reset_index()
  frequency['Co-Order %age  within same Order over Total Orders']= frequency.sum(axis=1)
  frequency= frequency[['name','Co-Order %age  within same Order over Total Orders']]
  frequency['Co-Order %age within same Order in Total Sales (Units) (In Store)']=volume['Co-Order %age within same Order in Total Sales (Units) (In Store)']
  productcombo= frequency.reset_index()[[                                                            'name',
        'Co-Order %age within same Order in Total Sales (Units) (In Store)',
                        'Co-Order %age  within same Order over Total Orders']]
  productcombo.columns=['prod1','Co-Order %age within same Order in Total Sales (Units) (In Store)','Co-Order %age  within same Order over Total Orders']
  productcombo['prod2']=productcombo['prod1']
  productcombo['category']='withproduct'
  productcombo=productcombo[['prod1', 'prod2','Co-Order %age  within same Order over Total Orders',
        'Co-Order %age within same Order in Total Sales (Units) (In Store)','category']]
  productcombo['store']=v
  productcombo['date']=today
  final_temp1=productcombo[productcombo['Co-Order %age  within same Order over Total Orders']>50]
  final_temp2=productcombo[productcombo['Co-Order %age within same Order in Total Sales (Units) (In Store)']>50]
  final=final_temp1.append(final_temp2)
  print('flag1')
  print(final)
  return final

"""**Within Same store**"""

def withinstore(v):
  dfsame = pd.read_sql("""SELECT order_id,products.name,item_qty FROM order_items
  left join orders on order_items.order_id= orders.id
  left join stores on stores.id=orders.store_id
  left join products on products.id=order_items.item_id
  where stores.name= "{v}";
    """.format(v=v), con=connn)
  order_count=dfsame.order_id.value_counts().reset_index()
  order_with_multiple_items= order_count[order_count['order_id']>1]['index']
  dfsame= dfsame[dfsame['order_id'].isin(list(order_with_multiple_items))]
  from itertools import combinations 
  def rSubset(arr, r): 
      return list(combinations(arr, r))
  finalcombo=[]
  for i in dfsame['order_id'].unique():
    r=2
    arr=list(dfsame[dfsame['order_id']==i]['name'])
    finalcombo.append(rSubset(arr, r))
  dffinal=pd.DataFrame()
  final_combo=pd.DataFrame(finalcombo)
  final_combo['order_id']=dfsame['order_id'].unique()
  a=0
  for i in range(len(final_combo['order_id'])):
    dftemp= final_combo[final_combo['order_id']==final_combo['order_id'][i]]
    dftemp=dftemp.dropna(axis=1)
    for j in dftemp.columns[:-1]:
      order_temp = dfsame[dfsame['order_id']==final_combo['order_id'][i]]
      order_temp=order_temp.sort_values('name')
      prodlist=list(dftemp[j][a])
      product1=prodlist[0]
      product2=prodlist[1]
      product1count= order_temp[order_temp['name']==product1]['item_qty'].values[0]
      product2count= order_temp[order_temp['name']==product2]['item_qty'].values[0]
      total=int(product1count+product2count)
      dffinal=dffinal.append([[product1,product2,total]])
    a+=1
  dffinal['Combo']=''
  for i in range(len(dffinal)):
    try:
      dffinal['Combo'][i]=dffinal[0][i]+'@@'+dffinal[1][i]
    except KeyError:
      pass
  productcombo = pd.crosstab(dffinal['Combo'],dffinal[2])
  productcombo=productcombo.reset_index()
  productcombo['Combo']= productcombo['Combo'].apply(lambda x: x.split('@@'))
  productcombo[['prod1','prod2']]=pd.DataFrame(productcombo['Combo'].to_list())
  productcombo=productcombo.drop('Combo',axis=1)
  frequency= productcombo.copy()
  volume=pd.DataFrame(frequency).copy()
  labels=['prod1', 'prod2']
  allcolumns= list(productcombo.columns)
  numberlabels = list(set(allcolumns)-set(labels))
  for i in numberlabels:
    volume[i]=''
  for i in numberlabels:
    volume[i]=frequency[i]*i
  dfsametwo = pd.read_sql("""SELECT products.name,item_qty FROM order_items
  left join products on order_items.item_id=products.id
  left join orders on orders.id=order_items.order_id
  left join stores on stores.id=orders.store_id
  where
  stores.name= "{v}";
    """.format(v=v), con=connn)
  dfsametwo= dfsametwo.groupby('name')['item_qty'].agg(['sum','count'])
  dfsametwo.columns=['volume','frequency']
  dfsametwo= dfsametwo.reset_index()
  frequency= frequency.merge(dfsametwo,left_on='prod1',right_on='name').merge(dfsametwo,left_on='prod2',right_on='name')
  volume= volume.merge(dfsametwo,left_on='prod1',right_on='name').merge(dfsametwo,left_on='prod2',right_on='name')
  frequency= frequency.drop(['name_x','volume_x','name_y','volume_y'],axis=1)
  frequency['prodtotal']=frequency['frequency_x']+frequency['frequency_y']
  frequency = frequency.drop(['frequency_x','frequency_y'],axis=1)
  frequency_temp=frequency.drop(['prod1','prod2'],axis=1)
  for i in frequency_temp.columns[:-1]:
    frequency_temp[i]=frequency_temp[i]/frequency_temp['prodtotal']*100
  frequency['Co-Order %age  within same Order over Total Orders']=frequency_temp.iloc[:,:-1].sum(axis=1)
  frequency=frequency[['prod1','prod2','Co-Order %age  within same Order over Total Orders']]
  volume= volume.drop(['name_x','frequency_x','name_y','frequency_y'],axis=1)
  volume['prodtotal']=volume['volume_x']+volume['volume_y']
  volume = volume.drop(['volume_x','volume_y'],axis=1)
  volume_temp=volume.drop(['prod1','prod2'],axis=1)
  for i in volume_temp.columns[:-1]:
    volume_temp[i]=volume_temp[i]/volume_temp['prodtotal']*100
  volume['Co-Order %age within same Order in Total Sales (Units) (In Store)']=volume_temp.iloc[:,:-1].sum(axis=1)
  volume=volume[['prod1','prod2','Co-Order %age within same Order in Total Sales (Units) (In Store)']]
  frequency['Co-Order %age within same Order in Total Sales (Units) (In Store)']=volume['Co-Order %age within same Order in Total Sales (Units) (In Store)']
  productcombo=frequency
  productcombo['Combo']=''
  for i in range(len(productcombo)):
    a=[productcombo['prod1'][i],productcombo['prod2'][i]]
    a.sort()
    productcombo['Combo'][i]=a
  for i in range(len(productcombo)):
    productcombo['Combo'][i]="@".join(productcombo['Combo'][i])
  productcombo= productcombo.groupby('Combo').sum().reset_index()
  for i in range(len(productcombo)):
    productcombo['Combo'][i]=productcombo['Combo'][i].split('@')
  productcombo[['prod1','prod2']]=pd.DataFrame(productcombo['Combo'].to_list())
  productcombo=productcombo[['prod1', 'prod2','Co-Order %age  within same Order over Total Orders',
        'Co-Order %age within same Order in Total Sales (Units) (In Store)']]
  productcombo['category']='withinstore'
  print('flag2')
  productcombo['store']=v
  productcombo['date']=today
  final_temp1=productcombo[productcombo['Co-Order %age  within same Order over Total Orders']>50]
  final_temp2=productcombo[productcombo['Co-Order %age within same Order in Total Sales (Units) (In Store)']>50]
  productcombo=final_temp1.append(final_temp2)
  print(productcombo)
  return productcombo

"""**External Corr**"""

def external(v):
  dfnext = pd.read_sql("""SELECT distinct item_qty,order_group_no,products.name FROM (SELECT order_id,item_id,item_qty,order_group_no FROM(SELECT order_id,item_id,item_qty FROM(SELECT * FROM order_items where order_id in(
  SELECT id from orders where order_group_no in (select distinct order_group_no FROM orders
  inner join stores on orders.store_id=orders.store_id
  where stores.name="{v}"))) AS A
  left join products on products.id=A.item_id) AS B
  LEFT JOIN (
  select distinct order_group_no,orders.id FROM orders
  inner join stores on orders.store_id=orders.store_id
  where stores.name="{v}") AS C
  ON order_id=C.id) AS D
  left join products on D.item_id=products.id
    """.format(v=v), con=connn)
  order_count=dfnext.order_group_no.value_counts().reset_index()
  order_with_multiple_items= order_count[order_count['order_group_no']>1]['index']
  dfnext= dfnext[dfnext['order_group_no'].isin(list(order_with_multiple_items))]
  from itertools import combinations 
  def rSubset(arr, r): 
      return list(combinations(arr, r))
  finalcombo=[]
  for i in dfnext['order_group_no'].unique():
    r=2
    arr=list(dfnext[dfnext['order_group_no']==i]['name'])
    finalcombo.append(rSubset(arr, r))
  dffinal=pd.DataFrame()
  final_combo=pd.DataFrame(finalcombo)
  final_combo['order_group_no']=dfnext['order_group_no'].unique()
  a=0
  for i in range(len(final_combo['order_group_no'])):
    dftemp= final_combo[final_combo['order_group_no']==final_combo['order_group_no'][i]]
    dftemp=dftemp.dropna(axis=1)
    for j in dftemp.columns[:-1]:
      order_temp = dfnext[dfnext['order_group_no']==final_combo['order_group_no'][i]]
      order_temp=order_temp.sort_values('name')
      prodlist=list(dftemp[j][a])
      product1=prodlist[0]
      product2=prodlist[1]
      product1count= order_temp[order_temp['name']==product1]['item_qty'].values[0]
      product2count= order_temp[order_temp['name']==product2]['item_qty'].values[0]
      total=int(product1count+product2count)
      dffinal=dffinal.append([[product1,product2,total]])
    a+=1
  # dffinal= dffinal.groupby([0,1]).sum().reset_index().sort_values(2,ascending=False)
  # dffinal
  dffinal['Combo']=''
  for i in range(len(dffinal)):
    try:
      dffinal['Combo'][i]=dffinal[0][i]+'@@'+dffinal[1][i]
    except KeyError:
      pass
  productcombo = pd.crosstab(dffinal['Combo'],dffinal[2])
  productcombo=productcombo.reset_index()
  productcombo['Combo']= productcombo['Combo'].apply(lambda x: x.split('@@'))
  productcombo[['prod1','prod2']]=pd.DataFrame(productcombo['Combo'].to_list())
  frequency= productcombo.copy()
  volume=pd.DataFrame(frequency).copy()
  labels=['prod1', 'prod2','Combo']
  allcolumns= list(productcombo.columns)
  numberlabels = list(set(allcolumns)-set(labels))
  for i in numberlabels:
    volume[i]=''
  for i in numberlabels:
    volume[i]=frequency[i]*i
  dfsametwo = pd.read_sql("""SELECT products.name,item_qty FROM order_items
  left join products on order_items.item_id=products.id
  left join orders on orders.id=order_items.order_id
  left join stores on stores.id=orders.store_id;
    """, con=connn)
  dfsametwo= dfsametwo.groupby('name')['item_qty'].agg(['sum','count'])
  dfsametwo.columns=['volume','frequency']
  dfsametwo= dfsametwo.reset_index()
  frequency= frequency.merge(dfsametwo,left_on='prod1',right_on='name').merge(dfsametwo,left_on='prod2',right_on='name')
  volume= volume.merge(dfsametwo,left_on='prod1',right_on='name').merge(dfsametwo,left_on='prod2',right_on='name')
  frequency= frequency.drop(['name_x','volume_x','name_y','volume_y'],axis=1)
  frequency['prodtotal']=frequency['frequency_x']+frequency['frequency_y']
  frequency = frequency.drop(['frequency_x','frequency_y'],axis=1)
  frequency_temp=frequency.drop(['prod1','prod2','Combo'],axis=1)
  for i in frequency_temp.columns[:-1]:
    frequency_temp[i]=frequency_temp[i]/frequency_temp['prodtotal']*100
  frequency['Co-Order %age  within same Order over Total Orders']=frequency_temp.iloc[:,:-1].sum(axis=1)
  frequency=frequency[['prod1','prod2','Co-Order %age  within same Order over Total Orders']]
  volume= volume.drop(['name_x','frequency_x','name_y','frequency_y'],axis=1)
  volume['prodtotal']=volume['volume_x']+volume['volume_y']
  volume = volume.drop(['volume_x','volume_y'],axis=1)
  volume_temp=volume.drop(['prod1','prod2','Combo'],axis=1)
  for i in volume_temp.columns[:-1]:
    volume_temp[i]=volume_temp[i]/volume_temp['prodtotal']*100
  volume['Co-Order %age within same Order in Total Sales (Units) (In Store)']=volume_temp.iloc[:,:-1].sum(axis=1)
  volume=volume[['prod1','prod2','Co-Order %age within same Order in Total Sales (Units) (In Store)']]
  frequency['Co-Order %age within same Order in Total Sales (Units) (In Store)']=volume['Co-Order %age within same Order in Total Sales (Units) (In Store)']
  productcombo=frequency
  productcombo['Combo']=''
  for i in range(len(productcombo)):
    a=[productcombo['prod1'][i],productcombo['prod2'][i]]
    a.sort()
    productcombo['Combo'][i]=a
  productcombo.columns
  for i in range(len(productcombo)):
    productcombo['Combo'][i]="@".join(productcombo['Combo'][i])
  productcombo= productcombo.groupby('Combo').sum().reset_index()
  for i in range(len(productcombo)):
    productcombo['Combo'][i]=productcombo['Combo'][i].split('@')
  productcombo[['prod1','prod2']]=pd.DataFrame(productcombo['Combo'].to_list())
  productcombo=productcombo[['prod1', 'prod2','Co-Order %age  within same Order over Total Orders',
        'Co-Order %age within same Order in Total Sales (Units) (In Store)']]
  productconsider = pd.read_sql("""SELECT * FROM (SELECT products.name,sum(item_qty) as ordertotal FROM order_items
  left join products on order_items.item_id=products.id
  left join orders on orders.id=order_items.order_id
  left join stores on stores.id=orders.store_id
  group by products.name) AS A
  where ordertotal > 10;
    """, con=connn)
  a=list(productconsider['name'])
  productcombo=productcombo[productcombo['prod1'].isin(a)]
  productcombo=productcombo[productcombo['prod2'].isin(a)]
  productcombo['category']='external'
  print('flag3')
  productcombo['store']=v
  productcombo['date']=today
  final_temp1=productcombo[productcombo['Co-Order %age  within same Order over Total Orders']>50]
  final_temp2=productcombo[productcombo['Co-Order %age within same Order in Total Sales (Units) (In Store)']>50]
  final=final_temp1.append(final_temp2)
  return final

from datetime import date

today = date.today()

finaltotal=pd.DataFrame()
for v in storename['name'].to_list():
  print(v)
  finale=pd.DataFrame()
  try:
    finale= withproduct(v).append(withinstore(v)).append(external(v))
  except KeyError:
    pass
  finaltotal=finaltotal.append(finale)

finaltotal=finaltotal[['prod1','prod2','Co-Order %age  within same Order over Total Orders',
'Co-Order %age within same Order in Total Sales (Units) (In Store)','category','store','date']]
finaltotal.columns=['prod1', 'prod2', 'frequency','volume',
       'category', 'store', 'date']

engine = create_engine("mysql+pymysql://tuadmin:Turant123@turantdb-instance.cdvlyair641e.ap-south-1.rds.amazonaws.com/stepupstats")
conn = engine.connect()

# # Read
# qwe = pd.read_sql('select * from users;', con=con) 
#print(yesterday_record)
# # Create new Table
finaltotal.to_sql(con=conn, name='correlation_matrix', if_exists='replace')

conn.close()